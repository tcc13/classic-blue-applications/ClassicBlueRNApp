import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';
import AppNavigationContainer from '../src/navigation';
import fetchMock from 'jest-fetch-mock';
import { act } from 'react-test-renderer';

fetchMock.enableMocks();

jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const mockPhotos = [
  {
    alt_description: null,
    blur_hash: 'L9SfeCk;icxa.TofkVkX%3aeRPWC',
    categories: [],
    color: '#D15E31',
    created_at: '2020-10-05T15:11:26-04:00',
    current_user_collections: [],
    description: null,
    height: 4500,
    id: '-IrU9jaEtDw',
    liked_by_user: false,
    likes: 193,
    links: {
      download: 'https://unsplash.com/photos/-IrU9jaEtDw/download',
      download_location: 'https://api.unsplash.com/photos/-IrU9jaEtDw/download',
      html: 'https://unsplash.com/photos/-IrU9jaEtDw',
      self: 'https://api.unsplash.com/photos/-IrU9jaEtDw',
    },
    promoted_at: '2020-10-08T06:47:46-04:00',
    sponsorship: null,
    updated_at: '2020-10-08T09:26:54-04:00',
    urls: {
      full:
        'https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjEzNTc0M30',
      raw:
        'https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEzNTc0M30',
      regular:
        'https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEzNTc0M30',
      small:
        'https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjEzNTc0M30',
      thumb:
        'https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjEzNTc0M30',
    },
    user: {
      accepted_tos: true,
      bio:
        "Some of my projects are now available for purchase as prints on products - if you'd like to check them out, just click the link above. If you'd like to support me, you can also consider a donation paypal.me/pmcze ||| www.instagram.com/pmcze",
      first_name: 'Paweł',
      id: 'ogQykx6hk_c',
      instagram_username: 'pmcze',
      last_name: 'Czerwiński',
      links: {
        followers: 'https://api.unsplash.com/users/pawel_czerwinski/followers',
        following: 'https://api.unsplash.com/users/pawel_czerwinski/following',
        html: 'https://unsplash.com/@pawel_czerwinski',
        likes: 'https://api.unsplash.com/users/pawel_czerwinski/likes',
        photos: 'https://api.unsplash.com/users/pawel_czerwinski/photos',
        portfolio: 'https://api.unsplash.com/users/pawel_czerwinski/portfolio',
        self: 'https://api.unsplash.com/users/pawel_czerwinski',
      },
      location: 'Poland',
      name: 'Paweł Czerwiński',
      portfolio_url: 'http://pmcze.redbubble.com',
      profile_image: {
        large:
          'https://images.unsplash.com/profile-1592328433409-d9ce8a5333eaimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=128&w=128',
        medium:
          'https://images.unsplash.com/profile-1592328433409-d9ce8a5333eaimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64',
        small:
          'https://images.unsplash.com/profile-1592328433409-d9ce8a5333eaimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32',
      },
      total_collections: 3,
      total_likes: 24430,
      total_photos: 849,
      twitter_username: null,
      updated_at: '2020-10-08T22:10:54-04:00',
      username: 'pawel_czerwinski',
    },
    width: 3000,
  },
];

describe('Root Navigator test', () => {
  test('Welcome screen', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    let logo, createAccountText;

    logo = await findByTestId('logo');
    expect(logo).toBeTruthy();

    createAccountText = await findByText('CRIAR CONTA');
    expect(createAccountText).toBeTruthy();

    const enterText = await findByText('ENTRAR');
    expect(enterText).toBeTruthy();
  });

  test('Navigation to feed', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    await act(async () => {
      const toClick = await findByText('ENTRAR');

      fireEvent(toClick, 'press');

      const newHeader = await findByTestId('feed');

      expect(newHeader).toBeTruthy();
    });
  });

  test('Navigation to Photo detail', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    await act(async () => {
      const toClick = await findByText('ENTRAR');

      fetchMock.mockResponseOnce(JSON.stringify(mockPhotos));

      fireEvent(toClick, 'press');

      const newHeader = await findByTestId('feed');

      expect(newHeader).toBeTruthy();

      const imageButton = await findByTestId('photo-0');

      expect(imageButton).toBeTruthy();

      fireEvent(imageButton, 'press');

      const photoDetail = await findByText('Detalhe da foto');

      expect(photoDetail).toBeTruthy();
    });
  });

  test('Navigation to See On The Wall Tutorial', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    await act(async () => {
      const toClick = await findByText('ENTRAR');

      fetchMock.mockResponseOnce(JSON.stringify(mockPhotos));

      fireEvent(toClick, 'press');

      const newHeader = await findByTestId('feed');

      expect(newHeader).toBeTruthy();

      const imageButton = await findByTestId('photo-0');

      expect(imageButton).toBeTruthy();

      fireEvent(imageButton, 'press');

      const photoDetail = await findByText('Detalhe da foto');

      expect(photoDetail).toBeTruthy();

      const seeOnTheWallButton = await findByTestId('see-on-the-wall');

      expect(seeOnTheWallButton).toBeTruthy();

      fireEvent(seeOnTheWallButton, 'press');

      const tutorial = await findByText('Tutorial');

      expect(tutorial).toBeTruthy();
    });
  });

  test('Navigation to See On The Wall', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    await act(async () => {
      const toClick = await findByText('ENTRAR');

      fetchMock.mockResponseOnce(JSON.stringify(mockPhotos));

      fireEvent(toClick, 'press');

      const newHeader = await findByTestId('feed');

      expect(newHeader).toBeTruthy();

      const imageButton = await findByTestId('photo-0');

      expect(imageButton).toBeTruthy();

      fireEvent(imageButton, 'press');

      const photoDetail = await findByText('Detalhe da foto');

      expect(photoDetail).toBeTruthy();

      const seeOnTheWallButton = await findByTestId('see-on-the-wall');

      expect(seeOnTheWallButton).toBeTruthy();

      fireEvent(seeOnTheWallButton, 'press');

      const tutorial = await findByText('Tutorial');

      expect(tutorial).toBeTruthy();

      const advanceButton = await findByText('AVANÇAR');

      expect(advanceButton).toBeTruthy();

      fireEvent(advanceButton, 'press');

      const seeOnTheWall = await findByText('Ver na parede');

      expect(seeOnTheWall).toBeTruthy();
    });
  });

  test('Take a photo from see on the wall and confirm save', async () => {
    const component = <AppNavigationContainer />;

    const { findByText, findByTestId } = render(component);

    await act(async () => {
      const toClick = await findByText('ENTRAR');

      fetchMock.mockResponseOnce(JSON.stringify(mockPhotos));

      fireEvent(toClick, 'press');

      const newHeader = await findByTestId('feed');

      expect(newHeader).toBeTruthy();

      const imageButton = await findByTestId('photo-0');

      expect(imageButton).toBeTruthy();

      fireEvent(imageButton, 'press');

      const photoDetail = await findByText('Detalhe da foto');

      expect(photoDetail).toBeTruthy();

      const seeOnTheWallButton = await findByTestId('see-on-the-wall');

      expect(seeOnTheWallButton).toBeTruthy();

      jest.spyOn(React, 'useRef').mockReturnValue({
        current: () => {
          return {
            url: '',
          };
        },
      });

      fireEvent(seeOnTheWallButton, 'press');

      const tutorial = await findByText('Tutorial');

      expect(tutorial).toBeTruthy();

      const advanceButton = await findByText('AVANÇAR');

      expect(advanceButton).toBeTruthy();

      fireEvent(advanceButton, 'press');

      let seeOnTheWall = await findByText('Ver na parede');

      expect(seeOnTheWall).toBeTruthy();

      const takePhotoButton = await findByTestId('take-photo-button');

      expect(takePhotoButton).toBeTruthy();

      fireEvent(takePhotoButton, 'press');

      const confirmCapture = await findByText('Salvar imagem');

      expect(confirmCapture).toBeTruthy();

      const saveCaptureButton = await findByTestId('save-button');

      expect(saveCaptureButton).toBeTruthy();

      fireEvent.press(saveCaptureButton);

      seeOnTheWall = await findByText('Ver na parede');

      expect(seeOnTheWall).toBeTruthy();
    });
  });
});

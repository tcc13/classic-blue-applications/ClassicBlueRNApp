module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  endOfLine: 'auto',
  trailingComma: 'all',
};

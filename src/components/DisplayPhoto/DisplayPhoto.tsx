import React from 'react';
import { Image, useWindowDimensions } from 'react-native';
import { spacing } from '../../theme';
import Photo from '../../ts/models/photo';

interface Props {
  photo: Photo;
}

/**
 * Display a given photo full width, minus horizontal padding.
 * Uses device width to calculate it.
 */
const DisplayPhoto: React.FC<Props> = ({ photo }) => {
  const { width } = useWindowDimensions();

  const imageWidth = width - 2 * spacing.horizontalSpace;

  const heightRatio = photo.height / photo.width;

  return (
    <Image
      style={{ width: imageWidth, height: heightRatio * imageWidth }}
      source={{ uri: photo.url }}
    />
  );
};

export default DisplayPhoto;

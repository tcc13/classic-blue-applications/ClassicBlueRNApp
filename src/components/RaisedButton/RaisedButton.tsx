import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import CustomText from '../CustomText/CustomText';
import { colors } from '../../theme';
import { ButtonProps } from '../../ts/interfaces';

const RaisedButton: React.FC<ButtonProps> = ({ text, onPress, testID }) => {
  return (
    <TouchableOpacity
      testID={testID}
      style={styles.raisedButtonContainer}
      onPress={onPress}>
      <CustomText color="white">{text}</CustomText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  raisedButtonContainer: {
    height: 48,
    backgroundColor: colors.primary.main,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    width: '100%',
  },
});

export default RaisedButton;

import React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle } from 'react-native';
import { colors } from '../../theme';

interface Props {
  size?: number;
  color?: string;
  children: string | React.FC;
  bold?: boolean;
  style?: StyleProp<TextStyle>;
}

const CustomText: React.FC<Props> = ({
  size,
  color,
  children,
  bold,
  style,
}) => {
  const fontFamilyStyle = { fontFamily: 'Roboto-Medium' };
  return (
    <Text
      style={[
        styles.textStyles,
        size ? { fontSize: size } : null,
        color ? { color } : null,
        bold ? fontFamilyStyle : null,
        style,
      ]}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  textStyles: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: colors.primary.main,
  },
});

export default CustomText;

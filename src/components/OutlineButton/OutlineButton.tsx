import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import CustomText from '../CustomText/CustomText';
import { colors } from '../../theme';
import { ButtonProps } from '../../ts/interfaces';

const OutlineButton: React.FC<ButtonProps> = ({ text, onPress }) => {
  return (
    <TouchableOpacity style={styles.outlineButtonContainer} onPress={onPress}>
      <CustomText color={colors.primary.main}>{text}</CustomText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  outlineButtonContainer: {
    height: 48,
    borderWidth: 1,
    borderColor: colors.primary.main,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    width: '100%',
  },
});

export default OutlineButton;

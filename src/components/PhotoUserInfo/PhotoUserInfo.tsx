import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import CustomText from '../CustomText/CustomText';
import { colors, spacing } from '../../theme';

interface Props {
  userProfileImage: string;
  userName: string;
}

const PhotoUserInfo: React.FC<Props> = ({ userProfileImage, userName }) => {
  return (
    <View style={styles.photoUserInfoWrapper}>
      {userProfileImage ? (
        <Image
          source={{ uri: userProfileImage }}
          style={styles.userProfileImage}
        />
      ) : (
        <FontAwesome name="user-circle" size={24} color={colors.primary.main} />
      )}
      <CustomText bold size={16} style={styles.userNameText}>
        {userName}
      </CustomText>
    </View>
  );
};

const styles = StyleSheet.create({
  photoUserInfoWrapper: { flex: 1, flexDirection: 'row', alignItems: 'center' },
  userNameText: {
    marginHorizontal: spacing.horizontalSpace,
    flex: 1,
    textTransform: 'capitalize',
  },
  userProfileImage: { height: 32, width: 32 },
});

export default PhotoUserInfo;

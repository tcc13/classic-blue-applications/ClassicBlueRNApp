import { StackNavigationProp } from '@react-navigation/stack';
import React, { FC } from 'react';
import {
  GestureResponderEvent,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { CustomText, DisplayPhoto, PhotoUserInfo } from '../../components';
import { colors, spacing } from '../../theme';
import Photo from '../../ts/models/photo';
import {
  PhotoDetailScreenProps,
  RootStackParamList,
} from '../../ts/navigation/rootStack';

const PhotoDetail: React.FC<PhotoDetailScreenProps> = ({
  route: {
    params: { photo },
  },
  navigation,
}) => {
  const { userProfileImage, userName, description } = photo;

  return (
    <SafeAreaView style={PhotoDetailStyles.PhotoDetailWrapper}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingVertical: spacing.verticalSpace,
          paddingHorizontal: spacing.horizontalSpace,
        }}>
        <View style={{ paddingBottom: spacing.verticalSpace / 2 }}>
          <PhotoUserInfo
            userProfileImage={userProfileImage}
            userName={userName}
          />
        </View>

        <DisplayPhoto photo={photo} />

        <PhotoDetailActions navigation={navigation} photo={photo} />

        <CustomText style={{ marginTop: spacing.verticalSpace / 2 }}>
          {description}
        </CustomText>
      </ScrollView>
    </SafeAreaView>
  );
};

const PhotoDetailStyles = StyleSheet.create({
  PhotoDetailWrapper: {
    flex: 1,
  },
});

interface PhotoDetailActionsProps {
  navigation: StackNavigationProp<RootStackParamList, 'PhotoDetail'>;
  photo: Photo;
}

const PhotoDetailActions: React.FC<PhotoDetailActionsProps> = ({
  navigation,
  photo,
}) => {
  return (
    <View>
      <PhotoDetailAction
        testID="see-on-the-wall"
        icon={() => (
          <MaterialIcons
            name="remove-red-eye"
            size={18}
            color={colors.primary.main}
          />
        )}
        text="VER NA PAREDE"
        firstAction
        lastAction
        onPress={() => navigation.navigate('QRCodeTutorial', { photo })}
      />
    </View>
  );
};

interface PhotoDetailActionProp {
  icon: FC;
  text: string;
  firstAction?: boolean;
  lastAction?: boolean;
  onPress?: (event: GestureResponderEvent) => void;
  testID?: string;
}

const PhotoDetailAction: React.FC<PhotoDetailActionProp> = ({
  icon: Icon,
  text,
  firstAction,
  lastAction,
  onPress,
  testID,
}) => {
  const customStyle = {
    borderLeftWidth: firstAction ? 1 : 0.5,
    borderRightWidth: lastAction ? 1 : 0.5,
  };

  return (
    <TouchableOpacity
      testID={testID}
      style={[PhotoDetailActionStyles.photoDetailActionWrapper, customStyle]}
      onPress={onPress}>
      <Icon />
      <CustomText bold>{text}</CustomText>
    </TouchableOpacity>
  );
};

const PhotoDetailActionStyles = StyleSheet.create({
  photoDetailActionWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    borderTopWidth: 1,
    borderBottomWidth: 1,

    borderColor: colors.primary.main,
  },
});

export default PhotoDetail;

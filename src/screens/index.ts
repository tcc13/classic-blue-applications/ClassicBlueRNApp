import ConfirmCapture from './ConfirmCapture/ConfirmCapture';
import Feed from './Feed/Feed';
import PhotoDetail from './PhotoDetail/PhotoDetail';
import SeeOnTheWall from './SeeOnTheWall/SeeOnTheWall';
import Welcome from './Welcome/Welcome';

export { Feed, Welcome, PhotoDetail, SeeOnTheWall, ConfirmCapture };

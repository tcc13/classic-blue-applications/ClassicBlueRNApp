import React, { FC } from 'react';
import { Image, View } from 'react-native';
import { QRCodePreviewStyles as styles } from './QRCodePreview.styles';

const QRCodePreview: FC = () => {
  return (
    <View style={styles.QRCodePreviewWrapper}>
      <View style={styles.QRCodePreviewRow}>
        <Image
          source={require('../../../../assets/images/QRCode.png')}
          style={styles.QRCodeImage}
          resizeMode="contain"
        />
      </View>
    </View>
  );
};

export default QRCodePreview;

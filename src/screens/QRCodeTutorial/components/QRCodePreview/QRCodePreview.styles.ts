import { StyleSheet } from 'react-native';
import { spacing } from '../../../../theme';

export const QRCodePreviewStyles = StyleSheet.create({
  QRCodePreviewWrapper: {
    flex: 1,
    paddingVertical: spacing.verticalSpace,
  },
  QRCodePreviewRow: {
    flexDirection: 'row',
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: spacing.horizontalSpace,
    paddingVertical: spacing.verticalSpace,
  },
  QRCodeImage: { height: '50%', width: '50%' },
});

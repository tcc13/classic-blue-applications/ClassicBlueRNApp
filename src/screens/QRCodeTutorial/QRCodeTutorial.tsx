import React, { FC, useCallback } from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import Share from 'react-native-share';
import { QRCodeBase64 } from '../../assets/images/QRCodeBase64';
import { CustomText, OutlineButton, RaisedButton } from '../../components';
import { spacing } from '../../theme';
import { QRCodeTutorialProps } from '../../ts/navigation/rootStack';
import QRCodePreview from './components/QRCodePreview/QRCodePreview';

const QRCodeTutorial: FC<QRCodeTutorialProps> = ({
  route: {
    params: { photo },
  },
  navigation,
}) => {
  const shareQRCode = useCallback(async () => {
    try {
      await Share.open({
        url: QRCodeBase64,
      });
    } catch (error) {
      console.log('error', error);
    }
  }, []);

  return (
    <SafeAreaView style={styles.QRCodeTutorialWrapper}>
      <View style={styles.instructionsWrapper}>
        <View>
          <CustomText bold size={16}>
            Tenha em mãos o código QR abaixo.
          </CustomText>
        </View>
        <View>
          <CustomText>
            &bull; Clique em obter código QR para imprimir ou compartilhar com
            um outro dispositivo
          </CustomText>
          <CustomText>
            &bull; Posicione o código QR onde deseja visualizar a imagem
          </CustomText>
          <CustomText>
            &bull; Por fim, clique em avançar e aponte a câmera para o código QR
            próximo a ele
          </CustomText>
        </View>

        <QRCodePreview />
      </View>
      <View>
        <OutlineButton
          text="AVANÇAR"
          onPress={() => navigation.navigate('SeeOnTheWall', { photo })}
        />
        <View style={{ marginBottom: spacing.verticalSpace }} />
        <RaisedButton text="OBTER CÓDIGO QR" onPress={shareQRCode} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  QRCodeTutorialWrapper: {
    flex: 1,
    paddingHorizontal: spacing.horizontalSpace,
    paddingVertical: spacing.verticalSpace,
  },
  instructionsWrapper: {
    flex: 1,
  },
  titleWrapper: {},
});

export default QRCodeTutorial;

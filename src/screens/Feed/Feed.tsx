import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  FlatList,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import UnsplashApi from '../../services/unsplashApi';
import { spacing } from '../../theme';
import { FeedScreenProps } from '../../ts/navigation/rootStack';
import { FeedCategories, FeedItem } from './components';

const Feed: React.FC<FeedScreenProps> = ({ navigation }) => {
  const [feed, setFeed] = useState([]);

  const findFeedPhotos = async () => {
    const response = await new UnsplashApi().findFeedPhotos();
    setFeed(response);
  };

  useEffect(() => {
    findFeedPhotos();
  }, []);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity style={{ paddingLeft: spacing.horizontalSpace }}>
          <FontAwesome name="user-circle" size={24} color="white" />
        </TouchableOpacity>
      ),
      headerTitle: () => (
        <TextInput
          placeholder="Procure fotos ou artistas"
          placeholderTextColor="rgba(255,255,255, .7)"
          style={styles.headerTitle}
        />
      ),
      headerRight: () => (
        <TouchableOpacity style={{ paddingRight: spacing.horizontalSpace }}>
          <MaterialIcons name="search" size={24} color="white" />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <SafeAreaView testID="feed" style={styles.feedWrapper}>
      <View style={{ paddingVertical: spacing.verticalSpace }}>
        <FeedCategories />
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: spacing.horizontalSpace }}
        data={feed}
        keyExtractor={(_, index) => `${index}`}
        renderItem={({ item: photo, index }) => {
          return (
            <FeedItem
              testID={`photo-${index}`}
              photo={photo}
              onImagePress={() => navigation.navigate('PhotoDetail', { photo })}
            />
          );
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  feedWrapper: {
    flex: 1,
  },
  headerTitle: {
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
  },
});

export default Feed;

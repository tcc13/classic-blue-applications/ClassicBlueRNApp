import React, { useState } from 'react';
import { Alert, StyleSheet, TouchableOpacity, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DisplayPhoto } from '../../../../components';
import PhotoUserInfo from '../../../../components/PhotoUserInfo/PhotoUserInfo';
import { colors, spacing } from '../../../../theme';
import { OnPressFunc } from '../../../../ts/interfaces';
import Photo from '../../../../ts/models/photo';

interface FeedItemProps {
  testID?: string;
  onImagePress?: OnPressFunc;
  photo: Photo;
}

const FeedItem: React.FC<FeedItemProps> = ({ testID, photo, onImagePress }) => {
  return (
    <View style={styles.feedItemWrapper}>
      <FeedItemHeader photo={photo} />

      <FeedItemImage testID={testID} photo={photo} onPress={onImagePress} />

      <FeedItemVote />
    </View>
  );
};

interface FeedItemHeaderProps {
  photo: Photo;
}

const FeedItemHeader: React.FC<FeedItemHeaderProps> = ({ photo }) => {
  const [selected, setSelected] = useState(false);

  return (
    <View style={styles.feedItemHeaderWrapper}>
      <PhotoUserInfo
        userProfileImage={photo.userProfileImage}
        userName={photo.userName}
      />
      <TouchableOpacity
        onPress={() => {
          setSelected(!selected);
          Alert.alert('Funcionalidade em construção!');
        }}>
        {selected ? (
          <FontAwesome name="bookmark" color={colors.primary.main} size={20} />
        ) : (
          <FontAwesome
            name="bookmark-o"
            color={colors.primary.main}
            size={20}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

interface FeedItemImageProps {
  testID?: string;
  onPress?: OnPressFunc;
  photo: Photo;
}

const FeedItemImage: React.FC<FeedItemImageProps> = ({
  testID,
  photo,
  onPress,
}) => {
  return (
    <TouchableOpacity testID={testID} onPress={onPress}>
      <DisplayPhoto photo={photo} />
    </TouchableOpacity>
  );
};

const FeedItemVote: React.FC = () => {
  return (
    <View style={styles.voteWrapper}>
      <TouchableOpacity
        style={[styles.voteButtonWrapper, styles.leftVoteButtonWrapper]}>
        <MaterialIcons
          name="arrow-downward"
          color={colors.primary.main}
          size={24}
        />
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.voteButtonWrapper, styles.rightVoteButtonWrapper]}>
        <MaterialIcons
          name="arrow-upward"
          color={colors.primary.main}
          size={24}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  feedItemWrapper: { marginBottom: spacing.verticalSpace },
  feedItemHeaderWrapper: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: spacing.verticalSpace / 2,
  },
  feedItemUserName: { marginHorizontal: spacing.horizontalSpace, flex: 1 },
  voteWrapper: { width: '100%', flexDirection: 'row' },
  voteButtonWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: colors.primary.main,
    padding: 8,
  },
  leftVoteButtonWrapper: {
    borderLeftWidth: 1,
    borderRightWidth: 0.5,
  },
  rightVoteButtonWrapper: {
    borderLeftWidth: 0.5,
    borderRightWidth: 1,
  },
});

export default FeedItem;

import React, { useState } from 'react';
import { FlatList, View } from 'react-native';
import { spacing } from '../../../../theme';
import FeedCategory from '../FeedCategory/FeedCategory';

const FeedCategories: React.FC = () => {
  const feedCategories = ['ALL', 'TRENDING', 'HOT', 'SPONSORED'];
  const [selectedCategory, setSelectedCategory] = useState(0);

  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      keyExtractor={(_, index) => `${index}`}
      horizontal
      contentContainerStyle={{
        paddingHorizontal: spacing.horizontalSpace,
      }}
      data={feedCategories}
      renderItem={({ item: category, index }) => {
        const isSelectedCategory = selectedCategory === index;''
        return (
          <FeedCategory
            category={category}
            isSelectedCategory={isSelectedCategory}
            setSelectedCategory={() => {
              setSelectedCategory(index);
            }}
          />
        );
      }}
      ItemSeparatorComponent={({ }) => (
        <View style={{ width: spacing.clickableSpace }} />
      )}
    />
  );
};

export default FeedCategories;

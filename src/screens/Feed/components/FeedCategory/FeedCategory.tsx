import React from 'react';
import { Alert, TouchableOpacity } from 'react-native';
import { CustomText } from '../../../../components';
import { colors } from '../../../../theme';

interface FeedCategoryProps {
  category: string;
  isSelectedCategory: boolean;
  setSelectedCategory: () => void;
}

const FeedCategory: React.FC<FeedCategoryProps> = (props) => {
  const { category, isSelectedCategory, setSelectedCategory } = props;

  return (
    <TouchableOpacity
      onPress={() => {
        setSelectedCategory();
      }}>
      <CustomText
        bold
        size={20}
        color={
          isSelectedCategory
            ? colors.primary.main
            : colors.secondary.main
        }
      >
        {category}
      </CustomText>
    </TouchableOpacity>
  );
}

export default FeedCategory;
import React from 'react';
import { Alert, Image, SafeAreaView, StyleSheet, View } from 'react-native';
import { OutlineButton, RaisedButton } from '../../components';
import { WelcomeScreenProps } from '../../ts/navigation/rootStack';

const Welcome: React.FC<WelcomeScreenProps> = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.welcomeWrapper}>
      <View style={styles.row}>
        <Image
          source={require('../../assets/images/classicBlueLogoCropped.png')}
          resizeMode="contain"
          testID="logo"
        />
      </View>

      <View style={styles.row}>
        <RaisedButton
          text="CRIAR CONTA"
          onPress={() => {
            Alert.alert('Funcionalidade em construção!');
          }}
        />
      </View>

      <View style={styles.row}>
        <OutlineButton
          text="ENTRAR"
          onPress={() => {
            navigation.navigate('Feed');
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  welcomeWrapper: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  row: { flexDirection: 'row', paddingHorizontal: 16, paddingVertical: 12 },
});

export default Welcome;

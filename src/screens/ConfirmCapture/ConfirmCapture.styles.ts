import { StyleSheet } from 'react-native';
import { colors, spacing } from '../../theme';

const ConfirmCaptureStyles = StyleSheet.create({
  confirmCaptureWrapper: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.secondary.main,
  },
  actionButtonsWrapper: {
    flex: 1,
    backgroundColor: colors.light.main,
    width: '100%',
    paddingHorizontal: spacing.horizontalSpace,
    justifyContent: 'center',
  },
});

export default ConfirmCaptureStyles;

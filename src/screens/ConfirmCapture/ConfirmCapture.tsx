import CameraRoll from '@react-native-community/cameraroll';
import React, { FC, useEffect, useState } from 'react';
import { Dimensions, Image, SafeAreaView, View } from 'react-native';
import { OutlineButton, RaisedButton } from '../../components';
import { Dimension } from '../../helpers/dimensionConverter/dimensionConverter';
import { spacing } from '../../theme';
import { ConfirmCaptureScreenProps } from '../../ts/navigation/rootStack';
import styles from './ConfirmCapture.styles';

const { width: screenWidth } = Dimensions.get('screen');

const ConfirmCapture: FC<ConfirmCaptureScreenProps> = ({
  route: {
    params: { uri },
  },
  navigation,
}) => {
  const [dimension, setDimension] = useState<Dimension>({
    width: 1,
    height: 1,
  });

  useEffect(() => {
    Image.getSize(uri, (width, height) => {
      setDimension({
        height,
        width,
      });
    });
  }, [uri]);

  return (
    <SafeAreaView style={styles.confirmCaptureWrapper}>
      <Image
        source={{ uri }}
        style={{
          width: screenWidth,
          height: (screenWidth * dimension.height) / dimension.width,
        }}
      />

      <View style={styles.actionButtonsWrapper}>
        <OutlineButton
          testID="discard-button"
          text="DESCARTAR"
          onPress={() => navigation.goBack()}
        />

        <View style={{ marginBottom: spacing.verticalSpace }} />

        <RaisedButton
          testID="save-button"
          text="SALVAR"
          onPress={() => {
            CameraRoll.save(uri, { album: 'Classic Blue' });

            navigation.goBack();
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default ConfirmCapture;

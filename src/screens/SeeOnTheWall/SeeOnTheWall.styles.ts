import { StyleSheet } from 'react-native';
import { colors, spacing } from '../../theme';

const SeeOnTheWallStyles = StyleSheet.create({
  seeOnTheWallWrapper: { flex: 1 },
  paperSizeSelectorWrapper: { flexGrow: 0 },
  paperSizeSelectorItemText: {
    borderWidth: 1,
    borderColor: colors.primary.main,
    // borderRadius: 100,
    paddingHorizontal: 12,
    paddingVertical: 4,
    marginHorizontal: spacing.horizontalSpace / 2,
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  actionsWrapper: {
    alignItems: 'center',
    paddingHorizontal: spacing.horizontalSpace,
    paddingVertical: spacing.verticalSpace,
    width: '100%',
    backgroundColor: colors.light.main,
  },
  takePhotoButton: {
    marginTop: spacing.verticalSpace,
    height: 72,
    width: 72,
    backgroundColor: colors.light.main,
    borderWidth: 4,
    borderColor: colors.primary.main,
    borderRadius: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerTakePhotoButton: {
    height: 62,
    width: 62,
    backgroundColor: colors.primary.main,
    borderRadius: 31,
  },
});

export default SeeOnTheWallStyles;

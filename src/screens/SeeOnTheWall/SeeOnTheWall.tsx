import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  PaperSize,
  paperSizes,
} from '../../helpers/dimensionConverter/dimensionConverter';
import { colors } from '../../theme';
import { SeeOnTheWallScreenProps } from '../../ts/navigation/rootStack';
import ARView, { TakeScreenshot } from './components/ARView';
import styles from './SeeOnTheWall.styles';

const SeeOnTheWall: FC<SeeOnTheWallScreenProps> = ({
  route: {
    params: { photo },
  },
  navigation,
}) => {
  const [selectedPaperSize, setSelectedPaperSize] = useState<number>(1);
  const takeScreenshot = React.useRef<TakeScreenshot>();

  const PaperSizeSelectorItem:
    | ListRenderItem<PaperSize>
    | null
    | undefined = useCallback(
    ({ item, index }) => {
      const customStyle = {
        color:
          selectedPaperSize === index ? colors.light.main : colors.primary.main,
        backgroundColor:
          selectedPaperSize === index ? colors.primary.main : 'transparent',
      };

      return (
        <TouchableOpacity onPress={() => setSelectedPaperSize(index)}>
          <Text style={[styles.paperSizeSelectorItemText, customStyle]}>
            {item}
          </Text>
        </TouchableOpacity>
      );
    },
    [selectedPaperSize, setSelectedPaperSize],
  );

  const PaperSizeSelector = useCallback(() => {
    return (
      <FlatList
        keyExtractor={(item) => item}
        style={styles.paperSizeSelectorWrapper}
        horizontal
        data={paperSizes}
        renderItem={PaperSizeSelectorItem}
      />
    );
  }, [PaperSizeSelectorItem]);

  const Actions = () => {
    return (
      <View style={styles.actionsWrapper}>
        <PaperSizeSelector />
        <TouchableOpacity
          testID="take-photo-button"
          style={styles.takePhotoButton}
          onPress={async () => {
            if (!takeScreenshot.current) {
              return;
            }

            const { url } = await takeScreenshot.current(
              `${Date.now()}`,
              false,
            );

            navigation.navigate('ConfirmCapture', { uri: `file://${url}` });
          }}>
          <View style={styles.innerTakePhotoButton} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.seeOnTheWallWrapper}>
      <ARView
        photo={photo}
        paperSize={paperSizes[selectedPaperSize]}
        setTakeScreenshot={(takeScreenshotFunction) => {
          takeScreenshot.current = takeScreenshotFunction;
        }}
      />

      <Actions />
    </SafeAreaView>
  );
};

export default SeeOnTheWall;

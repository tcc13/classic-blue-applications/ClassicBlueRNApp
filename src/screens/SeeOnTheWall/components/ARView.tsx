import React, { FC, useCallback, useEffect, useState } from 'react';
import { View } from 'react-native';
import {
  ViroARImageMarker,
  ViroARScene,
  ViroARSceneNavigator,
  ViroARTrackingTargets,
  ViroBox,
  ViroMaterials,
} from 'react-viro';
import dimensionConverter, {
  Dimension,
  PaperSize,
} from '../../../helpers/dimensionConverter/dimensionConverter';
import Photo from '../../../ts/models/photo';
import ARViewStyles from './ARView.styles';

export interface TakeScreenshotResult {
  success: boolean;
  url: string;
  errorCode: number;
}

export type TakeScreenshot = (
  fileName: string,
  saveToCameraRoll: boolean,
) => Promise<TakeScreenshotResult>;

interface ARViewProps {
  photo: Photo;
  paperSize?: PaperSize;
  setTakeScreenshot: (takeScreenShot: TakeScreenshot) => void;
}

interface ARPhotoProps {
  photo: Photo;
  paperSize?: PaperSize;
}

interface ViroAppProps {
  photo: Photo;
  paperSize?: PaperSize;
}

interface SceneNavigator {
  viroAppProps: ViroAppProps;
  takeScreenshot: TakeScreenshot;
}

interface ARSceneProps {
  sceneNavigator: SceneNavigator;
}

const ARView: FC<ARViewProps> = ({
  photo,
  paperSize = 'A5',
  setTakeScreenshot,
}) => {
  const [loadedMaterials, setloadedMaterials] = useState(false);

  useEffect(() => {
    ViroARTrackingTargets.createTargets({
      qrcode: {
        source: require('../../../assets/images/QRCode.png'),
        orientation: 'Up',
        physicalWidth: 0.1, // real world width in meters
      },
    });

    ViroMaterials.createMaterials({
      photo: {
        diffuseTexture: { uri: photo.url },
      },
    });

    setloadedMaterials(true);
  }, [setloadedMaterials, photo]);

  const ARPhoto: FC<ARPhotoProps> = useCallback(
    ({ photo: photoProp, paperSize: paperSizeProp }) => {
      if (!loadedMaterials) {
        return <></>;
      }

      const dimension: Dimension = {
        width: photoProp.width,
        height: photoProp.height,
      };

      const { width, height }: Dimension = dimensionConverter(
        dimension,
        paperSizeProp,
      );

      return (
        <ViroBox
          position={[0, 0, 0]}
          scale={[width, 0, height]}
          materials={['photo']}
        />
      );
    },
    [loadedMaterials],
  );

  const ARScene: FC<ARSceneProps> = ({ sceneNavigator }) => {
    const {
      photo: photoProp,
      paperSize: paperSizeProp,
    } = sceneNavigator.viroAppProps;

    setTakeScreenshot(sceneNavigator.takeScreenshot);

    return (
      <ViroARScene>
        <ViroARImageMarker target={'qrcode'}>
          <ARPhoto photo={photoProp} paperSize={paperSizeProp} />
        </ViroARImageMarker>
      </ViroARScene>
    );
  };

  if (!loadedMaterials) {
    return <View style={ARViewStyles.ARViewEmptyPage} />;
  }

  return (
    <ViroARSceneNavigator
      initialScene={{ scene: ARScene }}
      viroAppProps={{
        photo,
        paperSize,
      }}
    />
  );
};

export default ARView;

import { StyleSheet } from 'react-native';

const ARViewStyles = StyleSheet.create({
  ARViewEmptyPage: { flex: 1, backgroundColor: 'black' },
});

export default ARViewStyles;

import Photo from '../ts/models/photo';

const unsplashAccessKey = 'BeTn_bBITtkEdaRHgXZYFTIGWShv14x-6_DfHJ1txcw';

export default class UnsplashApi {
  findFeedPhotos: any = async () => {
    try {
      const response = await fetch('https://api.unsplash.com/photos', {
        headers: {
          'Accept-Version': 'v1',
          Authorization: `Client-ID ${unsplashAccessKey}`,
        },
      });

      if (response.ok) {
        const jsonResponse = await response.json();

        return jsonResponse.map(
          ({
            width,
            height,
            urls: { regular: url },
            user: { name: userName, profile_image: profileImage },
            description,
          }: any) => {
            return new Photo(
              width,
              height,
              url,
              userName,
              profileImage ? profileImage.small : '',
              description,
            );
          },
        );
      }

      return [];
    } catch (error) {
      console.log('error', error);
    }
  };
}

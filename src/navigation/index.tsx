import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import {
  ConfirmCapture,
  Feed,
  PhotoDetail,
  SeeOnTheWall,
  Welcome,
} from '../screens';
import QRCodeTutorial from '../screens/QRCodeTutorial/QRCodeTutorial';
import { colors } from '../theme';
import { RootStackParamList } from '../ts/navigation/rootStack';

const RootStack = createStackNavigator<RootStackParamList>();

const AppNavigationContainer = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: colors.secondary.main,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontFamily: 'Roboto-Medium',
            fontSize: 18,
          },
        }}>
        <RootStack.Screen
          name="Welcome"
          component={Welcome}
          options={{ headerShown: false }}
        />
        <RootStack.Screen name="Feed" component={Feed} />
        <RootStack.Screen
          name="PhotoDetail"
          component={PhotoDetail}
          options={{ headerTitle: 'Detalhe da foto' }}
        />
        <RootStack.Screen
          name="SeeOnTheWall"
          component={SeeOnTheWall}
          options={{ headerTitle: 'Ver na parede' }}
        />
        <RootStack.Screen
          name="ConfirmCapture"
          component={ConfirmCapture}
          options={{ headerTitle: 'Salvar imagem' }}
        />
        <RootStack.Screen
          name="QRCodeTutorial"
          component={QRCodeTutorial}
          options={{ headerTitle: 'Tutorial' }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigationContainer;

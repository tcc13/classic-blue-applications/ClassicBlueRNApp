import dimensionConverter, { Dimension } from './dimensionConverter';

describe('Dimension Converter Test', () => {
  test('Image and paper in landscape that fits in width', () => {
    const dimension: Dimension = {
      width: 1920,
      height: 1080,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(1.189);
    expect(height).toBe(0.6688125);
  });

  test('Image and paper in landscape that fits in height', () => {
    const dimension: Dimension = {
      width: 1350,
      height: 1080,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(1.0512499999999998);
    expect(height).toBe(0.841);
  });

  test('Square image and paper in landscape that fits in height', () => {
    const dimension: Dimension = {
      width: 1080,
      height: 1080,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(0.841);
    expect(height).toBe(0.841);
  });

  test('Image and paper in portrait that fits in height', () => {
    const dimension: Dimension = {
      width: 1080,
      height: 1920,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(0.6688125);
    expect(height).toBe(1.189);
  });

  test('Image and paper in portrait that fits in width', () => {
    const dimension: Dimension = {
      width: 1080,
      height: 1350,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(0.841);
    expect(height).toBe(1.0512499999999998);
  });

  test('Square image and paper in portrait that fits in width', () => {
    const dimension: Dimension = {
      width: 1080,
      height: 1080,
    };

    const { width, height } = dimensionConverter(dimension, 'A0');

    expect(width).toBe(0.841);
    expect(height).toBe(0.841);
  });
});

/**
 * Dimensions is an interface that groups width and height.
 */
export interface Dimension {
  width: number;
  height: number;
}

export const paperSizes = ['A0', 'A1', 'A2', 'A3', 'A4', 'A5'] as const;

export type PaperSize = typeof paperSizes[number];

type PaperSizeMap = {
  [key in PaperSize]: Dimension;
};

// Paper size map in landscape orientation
const paperSizesMap: PaperSizeMap = {
  A0: { width: 1.189, height: 0.841 },
  A1: { width: 0.841, height: 0.594 },
  A2: { width: 0.594, height: 0.42 },
  A3: { width: 0.42, height: 0.297 },
  A4: { width: 0.297, height: 0.21 },
  A5: { width: 0.21, height: 0.148 },
};

/**
 * Returns if a given dimension is in Landscape orientation
 * @param dimesion Some dimension
 */
const isInLandscapeOrientation = (dimension: Dimension) => {
  return dimension.width > dimension.height;
};

/**
 * Dimension converter transforms a photo dimension into a real world size dimension
 * based on paper size
 * @param dimension The dimensions of the photo in pixels
 * @param paperSize Reference paper size for the conversion
 */
const dimensionConverter = (
  dimension: Dimension,
  paperSize: PaperSize = 'A1',
) => {
  let ratio: number,
    paperSizeRatio: number,
    paperSizeDimension: Dimension,
    realWorldDimension: Dimension;

  if (isInLandscapeOrientation(dimension)) {
    // ratio, paperSizeDimension and paperSizeRatio for landscape orientation
    ratio = dimension.height / dimension.width;
    paperSizeDimension = paperSizesMap[paperSize];
    paperSizeRatio = paperSizeDimension.height / paperSizeDimension.width;

    // If true, dimension fits width, otherwise fits height
    if (ratio < paperSizeRatio) {
      realWorldDimension = {
        width: paperSizeDimension.width,
        height: paperSizeDimension.width * ratio,
      };
    } else {
      realWorldDimension = {
        width: paperSizeDimension.height / ratio,
        height: paperSizeDimension.height,
      };
    }
  } else {
    // ratio, paperSizeDimension and paperSizeRatio for portrait orientation
    ratio = dimension.width / dimension.height;
    // Change to portrait orientation
    paperSizeDimension = {
      height: paperSizesMap[paperSize].width,
      width: paperSizesMap[paperSize].height,
    };
    paperSizeRatio = paperSizeDimension.width / paperSizeDimension.height;

    // If true, dimension fits height, otherwise fits width
    if (ratio < paperSizeRatio) {
      realWorldDimension = {
        height: paperSizeDimension.height,
        width: paperSizeDimension.height * ratio,
      };
    } else {
      realWorldDimension = {
        width: paperSizeDimension.width,
        height: paperSizeDimension.width / ratio,
      };
    }
  }

  return realWorldDimension;
};

export default dimensionConverter;

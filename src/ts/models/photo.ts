export default class Photo {
  width: number;
  height: number;
  url: string;
  userName: string;
  userProfileImage: string;
  description: string;

  constructor(
    width: number,
    height: number,
    url: string,
    userName: string,
    userProfileImage: string,
    description: string,
  ) {
    this.width = width;
    this.height = height;
    this.url = url;
    this.userName = userName;
    this.userProfileImage = userProfileImage;
    this.description = description;
  }
}

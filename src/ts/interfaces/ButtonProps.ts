import { OnPressFunc } from '.';

export interface ButtonProps {
  text: string;
  onPress?: OnPressFunc;
  testID?: string;
}

import { GestureResponderEvent } from 'react-native';

export interface OnPressFunc {
  (event: GestureResponderEvent): void;
}

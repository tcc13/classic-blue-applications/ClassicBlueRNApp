import { ButtonProps } from './ButtonProps';
import { OnPressFunc } from './OnPressFunc';

export type { ButtonProps, OnPressFunc };

import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import Photo from '../models/photo';

/**
 * Root Stack Screens param list
 */
export type RootStackParamList = {
  Welcome: undefined;
  Feed: undefined;
  PhotoDetail: { photo: Photo };
  SeeOnTheWall: { photo: Photo };
  ConfirmCapture: { uri: string };
  QRCodeTutorial: { photo: Photo };
};

/**
 * Welcome screen types
 */
type WelcomeScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Welcome'
>;

export type WelcomeScreenProps = {
  navigation: WelcomeScreenNavigationProp;
};

/**
 * Feed screen types
 */
type FeedScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Feed'>;

export type FeedScreenProps = {
  navigation: FeedScreenNavigationProp;
};

/**
 * PhotoDetail screen types
 */
type PhotoDetailScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'PhotoDetail'
>;

type PhotoDetailScreenRouteProp = RouteProp<RootStackParamList, 'PhotoDetail'>;

export type PhotoDetailScreenProps = {
  navigation: PhotoDetailScreenNavigationProp;
  route: PhotoDetailScreenRouteProp;
};

/**
 * SeeOnTheWall screen types
 */
type SeeOnTheWallScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'PhotoDetail'
>;

type SeeOnTheWallScreenRouteProp = RouteProp<RootStackParamList, 'PhotoDetail'>;

export type SeeOnTheWallScreenProps = {
  navigation: SeeOnTheWallScreenNavigationProp;
  route: SeeOnTheWallScreenRouteProp;
};

/**
 * ConfirmCapture screen types
 */
type ConfirmCaptureScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'ConfirmCapture'
>;

type ConfirmCaptureScreenRouteProp = RouteProp<
  RootStackParamList,
  'ConfirmCapture'
>;

export type ConfirmCaptureScreenProps = {
  navigation: ConfirmCaptureScreenNavigationProp;
  route: ConfirmCaptureScreenRouteProp;
};

/**
 * QRCodeTutorial screen types
 */
type QRCodeTutorialNavigationProp = StackNavigationProp<
  RootStackParamList,
  'QRCodeTutorial'
>;

type QRCodeTutorialRouteProp = RouteProp<RootStackParamList, 'QRCodeTutorial'>;

export type QRCodeTutorialProps = {
  navigation: QRCodeTutorialNavigationProp;
  route: QRCodeTutorialRouteProp;
};

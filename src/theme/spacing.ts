const spacing = {
  verticalSpace: 24,
  horizontalSpace: 16,
  clickableSpace: 24,
};

export default spacing;

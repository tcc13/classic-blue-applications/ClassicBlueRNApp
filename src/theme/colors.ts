interface Color {
  main: string;
  text: string;
  dark: string;
  light: string;
}

export interface Colors {
  primary: Color;
  secondary: Color;
  light: Color;
}

const colors: Colors = {
  primary: {
    main: 'rgba(15, 76, 129, 1)',
    text: 'white',
    dark: 'rgba(0, 37, 84, 1)',
    light: 'rgba(76, 119, 177, 1)',
  },
  secondary: {
    main: 'rgba(159, 183, 205, 1)',
    text: 'black',
    dark: 'rgba(112, 135, 156, 1)',
    light: 'rgba(209, 233, 255, 1)',
  },
  light: {
    main: 'rgba(255,255,255,1)',
    text: 'black',
    dark: 'rgba(204, 204, 204,1)',
    light: 'rgba(255,255,255,1)',
  },
};

export default colors;

import fetchMock from 'jest-fetch-mock';

fetchMock.enableMocks();

jest.mock('react-native-gesture-handler', () => {
  const View = require('react-native/Libraries/Components/View/View');

  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
    /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
    /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(),
    Directions: {},
  };
});

jest.mock('react-viro', () => {
  const View = require('react-native/Libraries/Components/View/View');

  return {
    ViroARImageMarker: View,
    ViroARScene: View,
    ViroARSceneNavigator: View,
    ViroARTrackingTargets: {
      createTargets: () => {},
    },
    ViroBox: View,
    ViroMaterials: {
      createMaterials: () => {},
    },
  };
});

jest.mock('react-native-reanimated', () => {});
// jest.mock('react-native-tab-view', () => {});

jest.mock('@react-native-community/cameraroll', () => {
  return {
    save: () => {},
  };
});

jest.mock('react-native-share', () => ({
  default: jest.fn(),
}));
